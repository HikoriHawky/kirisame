- ```sh
  whisper-faster.exe --model "large-v2" --vad_min_speech_duration_ms 100 --vad_max_speech_duration_s 10 --vad_filter True --vad_threshold 0.15 --vad_min_silence_duration_ms 3000 --output_format srt --language ja *.mp3
  ```
- ```sh
  whisper-faster.exe --model "large-v2" --output_format srt --language ja *.mp3
  ```
-