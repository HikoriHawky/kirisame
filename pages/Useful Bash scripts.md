- ```sh
  for file in *.xhtml; do mv "$file" "${file%.xhtml}.html"; done
  ```
- ```sh
  for file in *.html; do mv "$file" "${file%.html}.xhtml"; done
  ```
- ```sh
  whisperx --device cuda --hf_token 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 --align_model "jonatasgrosman/wav2vec2-large-xlsr-53-japanese" --model_dir "C:\bin\whisper-standalone\_models" --model large-v2-mix-jp --compute_type int8 --batch_size 1 --output_format vtt --highlight_words True --language ja
  ```
- ```sh
  whisper-ctranslate2.exe --model_directory "C:\bin\whisper-standalone\_models\faster-whisper-large-v2-mix-jp" --vad_min_speech_duration_ms 250 --vad_filter True --vad_threshold 0.15 --vad_min_silence_duration_ms 3000 --output_format srt --compute_type int8 --language ja
  ```
- ```sh
  whisper-faster.exe --model "medium" --vad_min_speech_duration_ms 100 --vad_max_speech_duration_s 10 --vad_filter True --vad_threshold 0.15 --vad_min_silence_duration_ms 3000 --output_format srt --compute_type int8 --language ja
  ```
- ```sh
  whisper-cpp.exe --output-vtt --language "ja" --model "C:\bin\whisper-standalone\models\ggml-jp.bin"
  ```
- ```sh
  whisperx --device cuda --hf_token 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 --no_align --model_dir "C:\bin\whisper-standalone\_models" --model large-v2-mix-jp --compute_type int8 --batch_size 1 --output_format vtt --highlight_words True --language ja
  e 1 --output_format vtt --highlight_words True --language ja
  ```
-