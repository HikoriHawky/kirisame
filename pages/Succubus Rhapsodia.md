# Project 2 Trans
	- Translates the .rxdata stuffs!
	- WIP
	- Not Exported
	- Exported
		- CommonEvents
		- 001-003
		- 010-053
		- 061-064
			- 066-095
		- Map876-887
		- 889-893
		- 894-908
	- Trying automated word wrap, length 80, tag yellow
		- Not Exported
			- 242-700
		- Exported
			- 096-145
			- 146-241
- Skill/Ability mods
	- file: O_MOD_RPG_Class_習得スキル.js
	- when 2 # 主人公 - put stuff here
	- 0: Skill, 1: Ability
	- ```js
	  # Lv1. Mod skills
	            group.push([1, 0, 17]) # Embrace
	  	  group.push([1, 0, 101]) # Tease
	  	  group.push([1, 0, 104]) # Trick Raid
	  	  group.push([1, 0, 114]) # Flattenize
	  	  group.push([1, 0, 125]) # refresh
	  	  group.push([1, 0, 165]) # Iris seed Alda
	  	  group.push([1, 0, 175]) # Nerine Blum/Nerineblum
	  	  group.push([1, 0, 177]) # Nerinaisa/Nerineiza
	  	  group.push([1, 0, 177]) # Nerina The Arda/Nerine Alda
	  	  group.push([1, 0, 181]) # El Daiza/Eldaiza
	  	  group.push([1, 0, 183]) # Safrabrum
	  	  group.push([1, 0, 185]) # Safraiza
	  	  group.push([1, 0, 189]) # Corioza
	  	  group.push([1, 0, 191]) # Asta Blum/Astable
	  	  group.push([1, 0, 193]) # Astaiza/Aster
	  	  group.push([1, 0, 200]) # Charm
	  	  group.push([1, 0, 201]) # Paid Charm
	  	  group.push([1, 0, 202]) # Lust
	  	  group.push([1, 0, 203]) # Paid Lust
	  	  group.push([1, 0, 215]) # Trim route/Trimroot
	  	  group.push([1, 0, 216]) # Trim Stoke/Trimstoke
	  	  group.push([1, 0, 241]) # Cooking
	  	  group.push([1, 0, 368]) # Recover
	  	  group.push([1, 0, 422]) # Uplifting kiss
	  	  group.push([1, 0, 613]) # Passion Beat
	  	     # Lv1. Mod abilities
	  	     group.push([1, 1, 70]) # Energy Drain?
	  	  group.push([1, 1, 104]) # Cooking knowledge
	  	  group.push([1, 1, 105]) # Magical knowledge
	  	  group.push([1, 1, 110]) # Courage
	  	  group.push([1, 1, 112]) # Flexible
	  	  group.push([1, 1, 124]) # Skilled
	  	  group.push([1, 1, 126]) # Desperation/Craving (Gust when)
	  	  group.push([1, 1, 152]) # Devil's constitution
	  	  group.push([1, 1, 210]) # EP Healing
	  	  group.push([1, 1, 212]) # Resilience
	  	  group.push([1, 1, 213]) # Recovery aura
	  	  group.push([1, 1, 214]) # Vitality
	  	  group.push([1, 1, 215]) # Overflowing Vitality
	  	  group.push([1, 1, 220]) # Experience Gain
	  	  group.push([1, 1, 221]) # Collection
	  	  group.push([1, 1, 222]) # Fortune
	  	  group.push([1, 1, 223]) # Hidden Passage
	  	  group.push([1, 1, 224]) # Dowsing
	  	  group.push([1, 1, 226]) # Doubt/Cheat
	  	  group.push([1, 1, 227]) # Prepared for surprise attack
	  	  group.push([1, 1, 240]) # Speedy Harvester
	  	  group.push([1, 1, 241]) # Savvy Harvester
	  	  group.push([1, 1, 242]) # Intensive harvest
	  	  group.push([1, 1, 243]) # Mass harvester
	  	  group.push([1, 1, 305]) # Embrace - Close hold
	  	  group.push([1, 1, 309]) # Flutterize/flattenize - kiss hold
	  	  group.push([1, 1, 340]) # Body language
	  	  group.push([1, 1, 352]) # Bottomless Stamina
	  	  group.push([1, 1, 376]) # Unconcious Chant
	  	  group.push([1, 1, 559]) # Arval: 1/4 VP cost of magic in combat
	  	  group.push([1, 1, 585]) # Something something crown: 1/5 VP cost of magic in combat and decreases VP cost
	  	  group.push([1, 1, 593]) # Battle Maiden: Decreases climax VP cost, and increases SS rate for each climax
	  ```