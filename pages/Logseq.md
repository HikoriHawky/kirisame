- Using [logseq-flow-nord](https://github.com/henices/logseq-flow-nord/tree/main/src) as theme for Desktop
- [[Logseq]] custom.css snippet for fonts? `test`
	- ```css
	  @import url('https://raw.githack.com/henices/logseq-flow-nord/main/src/palettes/nord-custom.css');
	  
	  @font-face {
	      font-family: AtHyper;
	      src: url('fonts/AtkinsonHyperlegible-Regular.ttf') format("truetype");
	  }
	  @font-face {
	      font-family: MonoLisa;
	      src: url('fonts/MonoLisaCustom-Regular.ttf') format("truetype");
	  }
	  
	  :root {
	    --cl-font-family: "Atkinson Hyperlegible", "AtHyper", "Noto Sans JP", sans-serif;
	    --cl-font-family-code: "Monolisa Custom", "MonoLisa", "Cascadia Code", monospace;
	    --cl-font-size: 18px;
	    --cl-marker-size: 18px;
	    --cl-header-font: "Atkinson Hyperlegible", "AtHyper", "Noto Sans JP", sans-serif;
	    --cl-title-font: "Atkinson Hyperlegible", "AtHyper", "Noto Sans JP", sans-serif;
	  }
	  
	  
	  .kef-doc-exported #main-content-container {
	    margin: 0 !important;
	    border: 0 !important;
	    border-radius: 0 !important;
	    box-shadow: none !important;
	    background-color: rgb(47, 53, 65) !important;
	  }
	  
	  .CodeMirror pre.CodeMirror-line {
	    font-size: 14px;
	  }
	  
	  .dark-theme .cm-s-solarized {
	    --ctp-rosewater: 244, 219, 214;
	    --ctp-flamingo: 240, 198, 198;
	    --ctp-pink: 245, 189, 230;
	    --ctp-mauve: 198, 160, 246;
	    --ctp-red: 237, 135, 150;
	    --ctp-maroon: 238, 153, 160;
	    --ctp-peach: 245, 169, 127;
	    --ctp-yellow: 238, 212, 159;
	    --ctp-green: 166, 218, 149;
	    --ctp-teal: 139, 213, 202;
	    --ctp-sky: 145, 215, 227;
	    --ctp-sapphire: 125, 196, 228;
	    --ctp-blue: 138, 173, 244;
	    --ctp-lavender: 183, 189, 248;
	    --ctp-text: 197, 207, 245;
	    --ctp-subtext1: 179, 188, 224;
	    --ctp-subtext0: 161, 170, 203;
	    --ctp-overlay2: 143, 151, 183;
	    --ctp-overlay1: 125, 132, 162;
	    --ctp-overlay0: 108, 114, 141;
	    --ctp-surface2: 90, 95, 120;
	    --ctp-surface1: 72, 76, 100;
	    --ctp-surface0: 54, 58, 79;
	    --ctp-base: 36, 39, 58;
	    --ctp-mantle: 30, 32, 48;
	    --ctp-crust: 24, 25, 38;
	    --bg0: rgb(var(--ctp-base));
	    --bg1: rgb(var(--ctp-overlay1));
	    --bg4: rgb(var(--ctp-overlay0));
	    --fg: rgb(var(--ctp-text));
	    --fg3: rgb(var(--ctp-subtext0));
	    --gray: rgb(var(--ctp-subtext1));
	    --blue: rgb(var(--ctp-blue));
	    --yellow: rgb(var(--ctp-yellow));
	    --aqua: rgb(var(--ctp-blue));
	    --orange: rgb(var(--ctp-peach));
	    --pink: rgb(var(--ctp-pink));
	    --current-line: rgb(var(--ctp-surface0));
	    --selection: rgb(var(--ctp-surface2));
	    --atom: rgb(var(--ctp-lavender));
	    --cursor: rgb(var(--ctp-overlay1));
	    --keyword-old: rgb(var(--ctp-red));
	    --keyword: rgb(var(--ctp-pink));
	    --operator: rgb(var(--ctp-sky));
	    --number: rgb(var(--ctp-peach));
	    --definition: rgb(var(--ctp-blue));
	    --string: rgb(var(--ctp-green));
	  }
	  
	  .white-theme .cm-s-solarized {
	    --ctp-rosewater: 222, 149, 132;
	    --ctp-flamingo: 221, 120, 120;
	    --ctp-pink: 236, 131, 208;
	    --ctp-mauve: 136, 57, 239;
	    --ctp-red: 210, 15, 57;
	    --ctp-maroon: 230, 69, 83;
	    --ctp-peach: 254, 100, 11;
	    --ctp-yellow: 228, 147, 32;
	    --ctp-green: 64, 160, 43;
	    --ctp-teal: 23, 146, 153;
	    --ctp-sky: 4, 165, 229;
	    --ctp-sapphire: 32, 159, 181;
	    --ctp-blue: 42, 110, 245;
	    --ctp-lavender: 114, 135, 253;
	    --ctp-text: 76, 79, 105;
	    --ctp-subtext1: 92, 95, 119;
	    --ctp-subtext0: 108, 111, 133;
	    --ctp-overlay2: 124, 127, 147;
	    --ctp-overlay1: 140, 143, 161;
	    --ctp-overlay0: 156, 160, 176;
	    --ctp-surface2: 172, 176, 190;
	    --ctp-surface1: 188, 192, 204;
	    --ctp-surface0: 204, 208, 218;
	    --ctp-base: 239, 241, 245;
	    --ctp-mantle: 230, 233, 239;
	    --ctp-crust: 220, 224, 232;
	    --bg0: rgb(var(--ctp-base));
	    --bg1: rgb(var(--ctp-overlay1));
	    --bg4: rgb(var(--ctp-overlay0));
	    --fg: rgb(var(--ctp-text));
	    --fg3: rgb(var(--ctp-subtext0));
	    --gray: rgb(var(--ctp-subtext1));
	    --blue: rgb(var(--ctp-blue));
	    --yellow: rgb(var(--ctp-yellow));
	    --aqua: rgb(var(--ctp-blue));
	    --orange: rgb(var(--ctp-peach));
	    --current-line: rgb(var(--ctp-surface0));
	    --selection: rgb(var(--ctp-surface2));
	    --atom: rgb(var(--ctp-lavender));
	    --cursor: rgb(var(--ctp-overlay1));
	    --keyword-old: rgb(var(--ctp-red));
	    --keyword: rgb(var(--ctp-pink));
	    --operator: rgb(var(--ctp-sky));
	    --number: rgb(var(--ctp-peach));
	    --definition: rgb(var(--ctp-blue));
	    --string: rgb(var(--ctp-green));
	  }
	  
	  .cm-s-solarized span.cm-comment { color: var(--gray); }
	  .cm-s-solarized span.cm-atom { color: var(--number); }
	  .cm-s-solarized span.cm-number { color: var(--atom); }
	  
	  .teadsfsdmp .cm-s-solarized span.cm-comment.cm-attribute { color: #97b757; }
	  .teadsfsdmp .cm-s-solarized span.cm-comment.cm-def { color: #bc9262; }
	  .teadsfsdmp .cm-s-solarized span.cm-comment.cm-tag { color: #bc6283; }
	  .teadsfsdmp .cm-s-solarized span.cm-comment.cm-type { color: #5998a6; }
	  .cm-s-solarized span.cm-comment { color: var(--gray); }
	  .cm-s-solarized span.cm-meta { color: var(--blue); }
	  
	  .cm-s-solarized span.cm-property { color: var(--fg); }
	  .cm-s-solarized span.cm-qualifier { color: var(--aqua); }
	  .cm-s-solarized span.cm-attribute { color: var(--aqua); }
	  .cm-s-solarized span.cm-keyword { color: var(--keyword); }
	  .cm-s-solarized span.cm-builtin { color: var(--orange); }
	  .cm-s-solarized span.cm-string { color: var(--string); }
	  .cm-s-solarized span.cm-string-2 { color: var(--aqua); }
	  
	  .cm-s-solarized span.cm-variable { color: var(--fg); }
	  .cm-s-solarized span.cm-variable-2 { color: var(--fg); }
	  .cm-s-solarized span.cm-variable-3, .dark-theme .cm-s-solarized span.cm-type { color: var(--yellow); }
	  .cm-s-solarized span.cm-operator { color: var(--operator); }
	  .cm-s-solarized span.cm-callee { color: var(--fg); }
	  .cm-s-solarized span.cm-def { color: var(--definition); }
	  .tesjhaks .cm-s-solarized span.cm-bracket { color: #f8f8f2; }
	  .cm-s-solarized span.cm-tag { color: var(--orange); }
	  .teastset .cm-s-solarized span.cm-header { color: #ae81ff; }
	  .teafgdsfsa .cm-s-solarized span.cm-link { color: #ae81ff; }
	  .tseatst .cm-s-solarized span.cm-error { background: #f92672; color: #f8f8f0; }
	  
	  .white-theme,
	  html[data-theme='light'] {
	      --ct-bg-color-h: 220;
	      --ct-bg-color-s: 23%;
	      --ct-bg-color-l: 95%;
	      --ct-bg-color: 220,23%,85%;
	      --ct-text-color: 234,16%,35%;
	      --ct-primariy-color: 316, 73%, 69%;
	      --ct-secondary-color: 11 ,59% ,67%;
	      --ct-accent-color: 355, 76%, 59%;
	  }
	  
	  .dark-theme,
	  html[data-theme='dark'] {
	      --ct-bg-color-h: 232;
	      --ct-bg-color-s: 23%;
	      --ct-bg-color-l: 18%;
	      --ct-bg-color: 232,23%,28%;
	      --ct-text-color: 227, 68%, 88%;
	      --ct-primariy-color: 316, 74%, 85%;
	      --ct-secondary-color: 171, 47%, 69%;
	      --ct-accent-color: 234, 82%, 85%;
	  }
	  
	  .white-theme .logseq-tldraw {
	    --tl-grid: rgb(140, 143, 161);
	  }
	  
	  .dark-theme .logseq-tldraw {
	    --tl-grid: rgb(125, 132, 162);
	  }
	  ```
- ``` css
  
  .CodeMirror pre.CodeMirror-line {
    font-size: 16px;
  }
  ```
-