public:: false

- Prev: [[Saegusa - Images]] Next: [[Saegusa - Chapter 2]]
- <p>俺の名前は<ruby><rb>一</rb><rt>いち</rt></ruby><ruby><rb>条</rb><rt>じょう</rt></ruby><ruby><rb>卓</rb><rt>たく</rt></ruby><ruby><rb>也</rb><rt>や</rt></ruby></p>
	- My name is Ichijou Takuya.
- <p class="calibre2">普通の公立高校に通う、ごく普通の高校一年生だ。</p>
	- A very ordinary first-year high school student, attending an ordinary public high school.
- <p class="calibre2">運動が得意なわけでもなければ、何か特技があるわけでもない俺は、高校では帰宅部として、貴重な青春時代を出来るだけ自分のための時間にあてることにしている。</p>
	- I'm not good at sports, not good at any special skills either, and as a member of the high school's go-home club, I decided to spend much of my precious youth on myself.
- <p class="calibre2">その上で、まずは何をするにも世の中お金がなければ始まらないということで、俺は駅前からちょっと外れた所にあるコンビニでバイトを始めることにした。</p>
	- Besides, getting anywhere in today's society simply ain't possible without money,
- <p class="calibre2">駅前のコンビニだと忙しそうだし、何よりうちの高校の利用者も多いだろうから、出来るだけコッソリとバイトするためにもわざと少し外れた所にあるコンビニを選んだ。</p>
- <p class="calibre2">そうして俺は、週に三日か四日、授業が終わればすぐにそこのコンビニへ行き、そして夜までレジ打ちのバイトをする生活を送っている。</p>
- <p class="calibre2">まぁそんな、だからと言って特別何があるわけでもない、俺はそこら辺にいる普通の高校生として、青春をそれなりに<ruby><rb>謳</rb><rt>おう</rt></ruby><ruby><rb>歌</rb><rt>か</rt></ruby>しているのであった。</p>
- <p class="calibre2">だが、俺は普通でも、うちの高校には一つだけ普通じゃない点がある。</p>
  <p class="calibre2"><br class="main"/></p>
- <p class="calibre2">それは、同じクラスの<ruby><rb>三枝</rb><rt>さえぐさ</rt></ruby><ruby><rb>紫</rb><rt>し</rt></ruby><ruby><rb>音</rb><rt>おん</rt></ruby>の存在に他ならない。</p>
  <p class="calibre2"><br class="main"/></p>
- <p class="calibre2">容姿端麗、頭脳明晰、オマケに良いとこのお嬢様らしく、一年生ながら学校で一番の人気者である彼女の名は、実は学校を飛び越え日本中で知られている程有名だったりする。</p>
- <p class="calibre2"><ruby><rb>何故</rb><rt>なぜ</rt></ruby>なら、彼女は国民的アイドルグループに所属していた、元超売れっ子アイドルでもあるからだ。</p>
- <p class="calibre2">高校への進学を理由に突然アイドル活動を引退したことは、ニュースでやっていたぐらいだから<ruby><rb>流石</rb><rt>さすが</rt></ruby>に俺でも知っていたのだが、まさかそんな彼女と同じ学校で、しかも同じクラスになるなんて思いもしなかった。</p>
- <p class="calibre2">栗色のミディアムボブヘアーに、クリクリとした大きな瞳、透明感のある白い肌。背はそんなに高くないけれどスラッとした長い足が際立っており、正直、誰が見ても美人と評するのが三枝紫音という存在だった。</p>
- <p class="calibre2">そんなわけで、超が付く程の有名人な彼女は、当然入学初日から多くの人に囲まれており、噂によるとまだ入学して二週間ちょっとにもかかわらず、既に何人もの人に告白までされているらしい。</p>
- <p class="calibre2">しかし、そんな彼女の浮いた話は何一つ聞こえてくることはなく、引退しても尚アイドルのように、いつも周りに分け隔てなくニコニコと笑顔を振り<ruby><rb>撒</rb><rt>ま</rt></ruby>いているのであった。</p>
- <p class="calibre2"><br class="main"/></p>
- <p class="calibre2">けれども平凡な俺は、そんな彼女を中心に構成されたクラスの輪には入らなかった。</p>
- <p class="calibre2">というか、入りたくもなかった。</p>
- <p class="calibre2">否定はしないが、俺は俺のペースで周りとか気にせず生きてく方が楽だから、長いものに巻かれる彼らと俺とでは価値観が合わないというだけの話だ。</p>
- <p class="calibre2">アイドルなんて、雑誌やテレビを通して見ているだけで十分だし、俺達平凡な高校生にどうこう出来る相手じゃないだろって思ってしまう。</p>
- <p class="calibre2">そんな<ruby><rb>高</rb><rt>たか</rt></ruby><ruby><rb>嶺</rb><rt>ね</rt></ruby>の花に期待するなど、時間と体力の無駄でしかない。</p>
- <p class="calibre2">そんなわけで、俺は今日も学校が終わるとすぐに教室を飛び出し、いつも通りコンビニでバイトをしているのだった。</p>
- <p class="calibre2"><br class="main"/></p>
- <p class="calibre2">店内に、扉の開くメロディーが流れる。</p>
- <p class="calibre2">そのメロディーに合わせて、いつも通り「いらっしゃいませ〜」と声を発しながら入ってくるお客様を確認する。</p>
- <p class="calibre2">そこには、マスクをして縁の大きめな<ruby><rb>眼鏡</rb><rt>めがね</rt></ruby>をかけ、更にはキャスケットを深めに被った露骨に怪しい女性が立っていた。</p>
- <p class="calibre2">そしてその女性は、顔を隠すように少し下を向きながら、物凄い早歩きで店内へと入ってくる。</p>
- <p class="calibre2">店内には今そのお客様一人だけのため、俺はその露骨に怪しい女性の動きをしっかりと目で追った。</p>
- <p class="calibre2">彼女は買い物カゴにサラダと弁当をさっと入れると、そのまますぐに俺の待つレジへと早歩きでやってきた。</p>
- <p class="calibre2">入店からここまで、多分三十秒も<ruby><rb>経</rb><rt>た</rt></ruby>っていない。</p>
- <p class="calibre2">今ちゃんと食べたい弁当選びました？　と問いたいぐらいの<ruby><rb>早</rb><rt>はや</rt></ruby><ruby><rb>業</rb><rt>わざ</rt></ruby>だった。</p>
- <p class="calibre2">そんな挙動不審でかなり怪しい彼女だが、相手は女性だしそのぐらいで動じる俺ではない。問い詰めたい気持ちをぐっと<ruby><rb>堪</rb><rt>こら</rt></ruby>えながら至って普通に接客をする。</p>
- <p class="calibre2">「お弁当、あたためま──」</p>
- <p class="calibre2">「だ、大丈夫です！」</p>
- ![00007.jpg](../assets/00007_1687957662499_0.jpg)
-
- <p class="calibre2">俺が言い終えるより先に、食い気味に答えられる。</p>
- <p class="calibre2">何か急いでるのかな？　と思いつつ、俺は少し急いで集計を終わらせてあげる。</p>
- <p class="calibre2">「以上、七百三十八円になります」</p>
- <p class="calibre2">「こ、これで！」</p>
- <p class="calibre2">彼女は財布からシュバっと千円札を取り出して渡してくる。</p>
- <p class="calibre2">小銭を取り出す素振りも見せないため、そのままその千円で精算してお釣りを手渡した。</p>
- <p class="calibre2">すると彼女は、お釣りを渡す俺の手を両手で包みながら大切そうに小銭を受け取ると、慌ててお弁当を入れた袋を手に持ちそのまま店を出ていく────かと思いきや、扉の前で一度立ち止まると、ちょっと間を空けてからくるっとターンし、また店内へと入ってきた。</p>
- <p class="calibre2">そして今度は、ドリンクコーナーでお茶を手にすると、すぐさま再びレジへとやってきた。</p>
- <p class="calibre2">少し興奮しているのか、その迫力に若干驚いたが、まぁただの買い忘れだろうと思い俺は至って平静を装いながら再び接客する。</p>
- <p class="calibre2">「百二十八円になります。袋はお付けしますか？」</p>
- <p class="calibre2">「必要ないです！」</p>
- <p class="calibre2">そう言うと彼女は、財布からまたシュバっと千円札を取り出すと、そのままその千円札を差し出してくる。</p>
- <p class="calibre2">……いや、あの、さっきのお釣りで確実に今小銭あるでしょと思いながらも、お客様にそんなことは言えない俺は仕方なくその千円札で精算をする。</p>
- <p class="calibre2">そしてお釣りを手渡す俺の手を、彼女はまた両手で包み込む。そして、大切そうにお釣りを受け取ると、小銭でパンパンになった財布と共に今度は本当に足早に去っていった。</p>
- <p class="calibre2">「本当、いつも何したいんだろ──三枝さん」</p>
- <p class="calibre2"><br class="main"/></p>
- <p class="calibre2">俺は去っていく彼女の背中を眺めながら、そう<ruby><rb>呟</rb><rt>つぶや</rt></ruby>いた。</p>
- <p class="calibre2">本人は変装したつもりなのだろうが、レジを<ruby><rb>挟</rb><rt>はさ</rt></ruby>んで向かい合えば流石に彼女が同じクラスメイトの三枝紫音だってことぐらい分かるんだよなぁ。</p>
-
- <p class="calibre2">まぁそんなわけで、</p>
-
- <p class="calibre2">クラスメイトの元アイドルが、とにかく挙動不審なんです。</p>