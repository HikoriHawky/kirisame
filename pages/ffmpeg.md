- ```sh
  ffmpeg -i input.mp4 -c copy -map 0 -segment_time 00:20:00 -f segment -reset_timestamps 1 output%03d.mp4
  ```
- [[Whisper Transcription]]
- ```sh
  ffmpeg -ar 16000 -ac 1 -c:a pcm_s16le -i file.mp3 -y file.wav
  ```