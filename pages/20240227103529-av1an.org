:PROPERTIES:
:ID:       ea4ba567-788f-4aec-981e-f5a5b3541935
:END:
#+title: av1an
#+filetags: :codecs:

#+begin_src bash
  av1an -i "Tororo-test.mkv" --verbose --split-method av-scenechange -m lsmash -c mkvmerge --sc-downscale-height 1080 -e svt-av1 --force -v "--input-depth 10 --tune 2 --enable-tf 0 --enable-qm 1 --qm-min 0 --qm-max 15 --keyint -1 --scd 0 --lp 1 --irefresh-type 2 --crf 35 --preset 4 --film-grain 8 --film-grain-denoise 0" --pix-format yuv420p10le -f "-y" -a "-c:a copy" --set-thread-affinity 2 -w 6 -o "Tororo-test-av1.mkv"
  av1an -i "【EFT】Lv.8｜\!uta｜\!lawson｜\!galleria｜\!gofun｜箱を買って破産しました！かもです！【猫麦とろろ⧸個人Vtuber】-v2016184903.vod.mp4" --verbose --split-method av-scenechange -m lsmash -c mkvmerge --sc-downscale-height 1080 -e svt-av1 --force -v "--input-depth 10 --tune 2 --enable-tf 0 --enable-qm 1 --qm-min 0 --qm-max 15 --keyint -1 --scd 0 --lp 1 --irefresh-type 2 --crf 35 --preset 4 --film-grain 8 --film-grain-denoise 0" --pix-format yuv420p10le -f "-y" -a "-c:a copy" --set-thread-affinity 2 -w 6 -o "【EFT】Lv.8｜\!uta｜\!lawson｜\!galleria｜\!gofun｜箱を買って破産しました！かもです！【猫麦とろろ⧸個人Vtuber】-v2016184903.vod.mkv"
#+end_src

#+begin_src bash
whisper-ctranslate2.exe --live_transcribe true --threads 14 --language ja --task translate --live_volume_thresho
ld 0.05 --live_input_device 15 --model medium --vad_filter true
#+end_src
