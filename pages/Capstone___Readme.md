# Python code

Install using pip install or in a venv, either should work
## Requirements
	- - fastapi
	- - pydantic
	- - uvicorn[standard]
	- - openai
	- - tiktoken
## Usage

Running command-line application
```sh
python test.py 1 <string> # Sentence mode
python test.py 2 # Chat mode

uvicorn main:app # Runs JSON API

# In test folder
bash emotion_test.sh # Runs 5x5 emotion test and outputs to results/ folder
```
## Configuration

Prompts for system prompt, emotion recognition and context are all stored in the config/ folder.

Other user configuration is in the .env file. Make sure to enter a valid OpenAI key, otherwise the program would not function.
```sh
api_key = "<OPENAI API KEY>"
debug = True # Toggles debug messages
max_token_limit = 3096  # Max tokens in history, not as response
```
# Web Application
## Requirements and Usage

Use latest version of node.js and npm, then run the following commands.
```sh
npm install
npm run dev
```
## Caveats

Due to CSRF limitations, a proxy is set up to proxy requests through Vite to the Python API. As such, this would probably not work outside of a local machine without added configuration.

Do check `vite.config.ts`, that's where the proxy is set up.