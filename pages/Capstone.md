- Implement a button that toggles on/off the detected emotion? maybe put the button on the left of the textbox?
- [[Capstone/Chatbot Showcase Script?]]
- Wanted to do lots of automated testing, but my wallet...
- TODO: Add a certain thing to a certain emotion
- Mention how the sideeffect of using a model is that certain edge-cases could already be accounted for
- https://arxiv.org/abs/2302.13971
- https://arxiv.org/abs/2303.08774
- https://arxiv.org/abs/2101.00027
- [[Capstone/Charts]]
- During presentation make sure to mention that I had to do a proxy to allow the API to work due to CSRF restrictions. Do mention this, as setting it up on a service like Vercel or something probably wouldn't work without configuration
- tests - 5 sentences, tested 5 times each
- [[Capstone/Readme]]