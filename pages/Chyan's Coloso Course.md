- Chapter 3
  collapsed:: true
	- Focusing on the body, Chyan notes that the body is more important than the face
	- It is important to have a clear picture of a mannequin of what you're drawing in your mind
		- Keep it accurate, free-flowing(?) and quick
		- Without a clear picture, the drawing will be stiff and contain errors
	- The body could be broadly split into three elements, in order of importance:
	  collapsed:: true
		- Movement
		  collapsed:: true
			- Could refer to poses and how natural they are
		- Proportion
		  collapsed:: true
			- How the drawing is deformed would change depending on artstyle.
			  id:: 649552e4-46ec-4b3f-90ee-f46fad2cce21
			  collapsed:: true
				- Bigger head to body ratio, for one
			- A well-proportioned body goes well with good lines
		- Line
		  collapsed:: true
			- Affects how a char can be seen as "attractive"
			-
	- It doesn't matter how well the lines are drawn if the character looks stiff and the proportions are off
	  collapsed:: true
		- ![image.png](../assets/image_1687508065740_0.png)
		  Exhibit A: Spider pose Ikemen
		- You can always cover lines with clothes, but you can't hide a stiff pose and a really chonky head
	- This lesson focuses on proportions first; We need a body to apply movement to. The ratios aren't strict, you don't need to follow them exactly
	- "Realistic" proportions use a 8 head figure, 6-7 fits anime/casual more
	- Female body:
		- Head, 1:2, usually looks like 1:1.8 cause of hair and such
		- Neck, 2/3 of the head's height, from the bottom of the ears
		  collapsed:: true
			- The neck is at the back of the head, so much of it isn't visible from the front.
		- Shoulders, 1.5 head height (with 8 head figures, too broad for more casual/anime style. 1.2-1.3 for them)
			- This includes the shoulders and arm too
		- Ribs
			- Chest muscles split at approx 1/2 of head height, starting from the **collarbone**
			- Ribs themselves split at 2/3 of head height
		- The upper body is shaped more like a cube, lower more like a trapezoid, stretching outwards. Wider pelvises, and all.
		- "Just think of the belly button as being somewhere below the hips", or about 3 rectangles below the chest muscle split
			- Information about how tall the rectangles are are inexplicably missing
	- Male body:
		- Their shoulders are wider, so the body is the inverse of a females: broader shoulders, narrower hips
-